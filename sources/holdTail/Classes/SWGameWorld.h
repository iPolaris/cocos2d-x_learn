//
//  SWGameWorld.h
//  Holdtail
//
//  Created by 帅 印 on 13-12-02.
//
//

#ifndef __Holdtail__SWGameWorld__
#define __Holdtail__SWGameWorld__

#include <iostream>
#include "cocos2d.h"

//定义属性
typedef enum {
    tag_player
}tagWorld;

class SWGameWorld:public cocos2d::Layer{
public:
    static cocos2d::Scene *scene();
    static SWGameWorld *sharedWorld();
private:
    virtual bool init();
    CREATE_FUNC(SWGameWorld);
    
};
#endif /* defined(__Holdtail__SWGameWorld__) */
