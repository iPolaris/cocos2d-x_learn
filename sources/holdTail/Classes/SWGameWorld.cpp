//
//  SWGameWorld.cpp
//  Holdtail
//
//  Created by 帅 印 on 13-12-02.
//
//

#include "SWGameWorld.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "SWGameMap.h"

using namespace cocos2d;
using namespace CocosDenshion;

//声明静态变量
static SWGameWorld *SWGW;

SWGameWorld *SWGameWorld::sharedWorld(){
    if(SWGW != NULL){
        return SWGW;
    }
    return  NULL;
}


Scene *SWGameWorld::scene(){
    Scene *scene = Scene::create();
    SWGameWorld *layer = SWGameWorld::create();
    scene->addChild(layer);
    return scene;
}

//创建场景
bool SWGameWorld::init(){
    if( !Layer::init()){
        return false;
    }
    SWGW = this;
    
    //地图
    SWGameMap *map = SWGameMap::createMap("cloudbg.png","cloud04.png","cloud03.png","cloud02.png","cloud01.png","treesbg.png");
    addChild(map);
    
    return true;
}
