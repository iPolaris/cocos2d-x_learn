//
//  StaticData.cpp
//  Flappy_sewer
//
//  Created by iPolaris on 14-3-23.
//
//

#include "StaticData.h"
USING_NS_CC;
static StaticData* _shareData = NULL;
StaticData* StaticData::sharedStaticData(){
    if (NULL == _shareData) {
        _shareData = new StaticData();
        _shareData -> init();
    }
    return _shareData;
};

bool StaticData::init(){
    dataDic = __Dictionary::createWithContentsOfFile("staticdata.plist");
    return true;
};

StaticData::StaticData(){
    
}
StaticData::~StaticData()
{
    CC_SAFE_RELEASE_NULL(dataDic);
}
const char* StaticData::stringFromKey(std::string key){
    return dataDic->valueForKey(key)->getCString();
};