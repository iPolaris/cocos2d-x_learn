//
//  FlappySewerGame.h
//  Flappy_sewer
//
//  Created by iPolaris on 14-3-22.
//
//

#ifndef __Flappy_sewer__FlappySewerGame__
#define __Flappy_sewer__FlappySewerGame__

#include <iostream>

#endif /* defined(__Flappy_sewer__FlappySewerGame__) */
#include "cocos2d.h"
class FlappySewerGame : public cocos2d::Layer{
public:
    static cocos2d::Scene* createScence();
    virtual bool init();
    void initUI();
    CREATE_FUNC(FlappySewerGame);
    void gameStart(Ref * pSender);
};