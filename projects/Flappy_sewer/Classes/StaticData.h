//
//  StaticData.h
//  Flappy_sewer
//
//  Created by iPolaris on 14-3-23.
//
//

#ifndef __Flappy_sewer__StaticData__
#define __Flappy_sewer__StaticData__

#include <iostream>

#endif /* defined(__Flappy_sewer__StaticData__) */
#define STATIC_DATA_STRING(key) StaticData::sharedStaticData()->stringFromKey(key)
#include "cocos2d.h"
class StaticData : public cocos2d::Ref{
public:
    static StaticData* sharedStaticData();
    const char* stringFromKey(std::string key);
private:
    StaticData();
    ~StaticData();
    bool init();
protected:
    cocos2d::__Dictionary *dataDic;
};