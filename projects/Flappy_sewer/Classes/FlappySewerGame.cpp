//
//  FlappySewerGame.cpp
//  Flappy_sewer
//
//  Created by iPolaris on 14-3-22.
//
//

#include "FlappySewerGame.h"
#include "Resources.h"
#include "StaticData.h"
USING_NS_CC;

Scene* FlappySewerGame::createScence(){
    auto *scene = Scene::create();
    auto *layer = FlappySewerGame::create();
    scene->addChild(layer);
    return scene;
};

bool FlappySewerGame::init(){
    if (!Layer::init()) {
        return false;
    }
    initUI();
    return true;
};

void FlappySewerGame::initUI(){
    auto winSize = Director::getInstance()->getWinSize();
    
    //设置背景
    auto background = Sprite::create(STATIC_DATA_STRING("background"));
    background->setPosition(winSize.width/2, winSize.height/2);
    background->setScale(winSize.width/background->getContentSize().width,
                    winSize.height/background->getContentSize().height);
    addChild(background);
    
    auto logo = Sprite::create(STATIC_DATA_STRING("logo"));
    logo->setPosition(winSize.width/2, winSize.height/2 + logo->getContentSize().height *2);
    logo -> setTag(LOGO_TAG);
    addChild(logo);
    
    //添加开始按钮
    auto statbt = MenuItemImage::create(STATIC_DATA_STRING("startbutton"), STATIC_DATA_STRING("startbutton_pressed"), CC_CALLBACK_1(FlappySewerGame::gameStart, this));
    auto menu = Menu::create(statbt, NULL);
    menu -> setTag(START_BUTTON_TAG);
    addChild(menu);
    
    //添加分数
    auto score = LabelBMFont::create("0", STATIC_DATA_STRING("futura_font"));
    score -> setPosition(winSize.width/2, winSize.height*3/4);
    score -> setVisible(false);
    score -> setTag(SCORE_TAG);
    addChild(score);
    
    auto hero = Sprite::create(STATIC_DATA_STRING("hero"));
   // hero->setVisible(false);
    hero -> setPosition(winSize.width/2, winSize.height/2-100);
    
    Animation * heroAn = Animation::create();
    heroAn->addSpriteFrameWithFile(STATIC_DATA_STRING("hero"));
    heroAn->addSpriteFrameWithFile(STATIC_DATA_STRING("hero2"));
    heroAn->addSpriteFrameWithFile(STATIC_DATA_STRING("hero3"));
    heroAn->setDelayPerUnit(0.5f/3.0f);
    heroAn->setLoops(-1);
    Animate * heroAnimate = Animate::create(heroAn);
    hero -> runAction(CCRepeatForever::create(heroAnimate));
    addChild(hero,1);
}

void FlappySewerGame::gameStart(cocos2d::Ref *pSender){
    //设为不可见的
    getChildByTag(LOGO_TAG) -> setVisible(false);
    getChildByTag(START_BUTTON_TAG) -> setVisible(false);
    
    //设为可见
    getChildByTag(SCORE_TAG) -> setVisible(true);
    log("game start");
}